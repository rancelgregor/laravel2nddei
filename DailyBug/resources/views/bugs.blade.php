@extends('layouts.app')
@section('content')
<h1 class="text-center py-5">All Bugs</h1>
<div class="row">
@foreach($bugs as $indiv_bug)
<div class="text-center col-lg-3 my-2">
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">{{$indiv_bug->title}}</h4>
			<p class="card-text">{{$indiv_bug->body}}</p>
			<p class="card-text">{{$indiv_bug->category_id}}</p>
			<p class="card-text">{{$indiv_bug->status_id}}</p>
			<p class="card-text">{{$indiv_bug->user_id}}</p>
		</div>
	</div>
</div>
@endforeach
@endsection
	






