<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Todo;
use \App\Category;

class TodoController extends Controller
{
    //
    public function index(){

    	$tasks = Todo::all();
    	// dd($tasks);//dump and die

    	return view('tasks', compact('tasks')/*send ng data*/);
    }
    public function create(){

    	$categories = Category::all();
    	return view('add-tasks', compact('categories'));
    }

    /*419 nakalimutan lagyan ng csrf yung form natin*/
    public function store(Request $req/*pikachu*/){/*gagawin kapag data galing sa form*/
    	// dd($req);
    	$new_task = new Todo/*Model*/;
    	/*nasa database*/$new_task->title = $req->title/*pikachu*/;/*nasa form*/
    	$new_task->body = $req->body;
    	$new_task->category_id = $req->category_id;
    	$new_task->status_id = 1;
    	$new_task->user_id = 1;
    	$new_task->save();

    return redirect('/tasks'/*actual route*/);
    }
    public function destroy($id){
    	//find the data to delete
    	//delete
    	$taskToDelete = Todo::find($id);
    	$taskToDelete->delete();
    	return redirect('/tasks');
 //kapag specific lang sa loob ng Todo
    }

    public function markAsDone($id){
    	//Find the task to update
    	//Update
    	$taskToUpdate = Todo::find($id);
    	if($taskToUpdate->status_id == 3){
    		$taskToUpdate->status_id = 1;
    	}
    	else{
    		$taskToUpdate->status_id = 3;
    	}
    	$taskToUpdate->save();
    	return redirect('/tasks');
    }
}
